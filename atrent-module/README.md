# Modulo `FanLoop`

## Autore: atrent

## link
- [repo progetto](https://gitlab.di.unimi.it/sistemiembedded/embedded-rube-goldberg/-/tree/atrent-module/atrent-module)
- [homepage autore](http://atrent.it)

## Licenza: [AGPL](LICENSE)


## Immagini

Inizio
![inizio montaggio](img/2023-05-20_18.39.23.jpg)

Alpha
![versione alpha](img/2023-06-02_19.01.31.jpg)

## Input

TBD

(luce?)

## Output

- rotational (impulse)
- magnetico o aria spostata


## Schema elettrico

TBD


## BOM

- dupont vari
- [esp8266 (wemos)](http://www.wemos.cc/en/latest/d1/d1_mini.html)
- [TM1638](http://esphome.io/components/display/tm1638.html) (tastierino) - 3 gpio
- ventolamotore
- sbarretta con magneti
- relè con transistor - 1 gpio
- buzzer - 1 gpio
- fotoresistenza - 1 ANALOG gpio
- breadboard
- alimentatore breadboard
- piano in legno
- squadrette di protezione
- viti varie
- alimentatore 220V-6V per alimentatore breadboard


## Alimentazione necessaria

~10V jack standard arduino (c'è il regolatore per la breadboard)

## Topic MQTT (con semantica)

TBD

## Tipo di reset

software

TBD
