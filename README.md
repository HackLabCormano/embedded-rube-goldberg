# Modular Embedded Rube Goldberg machines

**Scopo**: creare un'installazione artistica composta da tanti mini-oggetti IoT con sensori e attuatori che formano una "catena di azionamento".

Il tutto deve essere **modulare**: tanti moduli accoppiabili **quasi** a piacimento (ovviamente dipende dal tipo di effetto - attuatore+sensore - usato per trasmettere l'evento di transizione).

Le interfacce fisiche di accoppiamento fra moduli diventano delle APPI (Application Programming Physical Interface).

Ispirazione:
- https://en.wikipedia.org/wiki/Rube_Goldberg_machine
- https://en.wikipedia.org/wiki/Rube_Goldberg
- (non sapevo ci fosse un inglese analogo https://en.wikipedia.org/wiki/W._Heath_Robinson)


## Caratteristiche desiderate

**Se possibile:**
- ogni step NON dovrebbe essere *one shot* ma dovrebbe poter essere resettato a comando, in modo da poter lasciare il tutto sempre in funzione (a ciclo continuo, quando arriva infondo si riprende dall'inizio)
- quindi niente roba che va ri-riempita/svuotata (serbatoi) a mano o roba da ri-caricare (fiammiferi, roba che esplode, ecc.)
- ogni step deve compiere la sua azione entro un TIMEOUT (diciamo 1 minuto, MAX!)
- IMPORTANTE è documentare bene il comportamento


## Coppie attuatore-sensore usabili, suggerimenti

Meglio se roba "visibile/udibile", così da fare scena.

Tanto per capirci:
- lampadina+fotoresistenza
- lampadina(incandescenza)+termistore/dht11
- geiger (per dare inizio a tutto?)
- motore che fa roteare un bilanciere con un magnete + reed che ne misura la velocità di rotazione (atrent's FanLoop)
- stepper/servo che abbassa un qualcosa dentro un sensore a fotocellula
- stepper/servo che inclina una ampolla a mercurio
- stepper/servo che inclina un tubo di plexiglass con dentro una biglia di materiale metallico/magnetico (leggibile via Hall o Reed) o colorata (leggibile via fotocellula/fotoresistenza)
- solenoid pusher che batte su un knocksensor
- buzzer+microfono
- laser+fumo+lasersensor
- accendino(?)+flamesensor
- ventolina + altraventolina (o ruota a pale)
- ventolina + bandierina (attaccata a potenziometro o microswitch)
- stepper/servo che avvicina/allontana un oggetto ad un sensore a ultrasuoni
- stepper/servo che muove un condensatore variabile ad aria
- stepper/servo che pizzica una corda che viene ascoltata da un pickup
- pompetta che riempie un contenitore di liquido+sensore livello (e poi ci vorrebbe altro meccanismo per svuotare in automatico)
- attuatore lineare + rotaryencoder (mediante braccetto)
- spruzzatore (anche di bolle di sapone) verso un sensore di umidità ("resettabile" con una ventolina che lo asciuga)

Per ispirazioni vedere i vari "kit" sensori/attuatori, tipo [elegoo](https://www.elegoo.com/en-it/products/elegoo-37-in-1-sensor-kit) e simili


## Aumento della RubeGoldberg-ness

Complicare i "protocolli" passandosi informazione dinamica invece che statica, ad es. non usare semplicemente un *reed* ed un magnete (su motore) in booleano, ma far ruotare magnete e misurare velocità contando impulsi dal *reed* e far scattare lo step al raggiungimento di una certa velocità.

Ogni modulo potrebbe implementare internamente un meccanismo di ["complicazione"](https://en.wikipedia.org/wiki/Complication_(horology)) a scopo anche solo visivo (es. un servo che muove un interruttore, simil [useless machine](https://en.wikipedia.org/wiki/Useless_machine)).


## Costruzione

Direi macro-moduli piccoli (tipo 30x30cm, dimensione miei cassetti, ma non è vincolante) in legno da accoppiare variamente per allungare la catena, ogni macro-modulo implementa almeno un anello della catena (ma internamente potrebbe avere una micro-catena di azionamenti), a discrezione del realizzatore, a seconda di estetica e funzionalità.

![modello board](ModelloBoard.png)

In figura, posizionamento standard delle linee di alimentazione (sx e dx) e zona designata (input/output) per lo scambio fisico degli eventi (azionamento/sensoristica).
Ad esempio su una direttrice (NS) ingressi e uscite fisiche, sull'altra (EO) le alimentazioni.

`TBD`: Prevedere sovrapponibilità (mediante distanziali?) per riporli?

Alimentazione: 5V (board) + 12V (f.e.m.), jack standard arduino o USB.
Si può pensare che a fianco dell'installazione artistica corrano una serie di prolunghe in cascata (daisy chain) a cui attaccare gli alimentatori necessari, così ogni costruttore si porta il proprio alimentatore dalla 220V.

Ogni modulo va documentato stile `man` (nome, descr, APPI in-out, stringhe topic MQTT, ecc.) seguendo un [template](Template.md).


## APPI (Application Programming Physical Interface)

Vanno classificati i "mezzi di comunicazione", ad es.:
- elettrico
- temperatura
- aria/vento
- luce (visibile/IR)
- suono (frequenza, intensità)
- magnetismo
- meccanico
- radiazioni (meglio di no)
- ...

Eventualmente si potranno definire eventuali sottoclassi.

Poi andranno elencate eventuali compatibilità non immediate: chiaro che elettrico-elettrico è immediata, mentre temperatura-vento un po' meno.

Ogni modulo deve avere anche un minimo di "interfaccia" locale, ad es. un pulsante per attivarlo/resettarlo (cfr. `trigger` in figura).

E forse qualche `display` (oled, lcd, led strip, TM1638, ink-paper)?


## Monitoraggio

(Quasi) ogni anello della catena dovrebbe essere gestito da un `esp` (anche lo 01 va bene, anche quelli con OLED così da far vedere cose) che manda feedback via MQTT in modo da poter tenere traccia anche via software (tipo una dashboard).

**Comunque anche Arduino (e altre board senza wifi) sono accettate.**

`TBD`: alcune coppie attuatore-sensore potrebbero essere anche solo elettriche se molto semplici?

Durante le demo viene fornito un accesso wifi (anche solo locale, senza Internet?) e l'url di un broker MQTT a cui collegarsi.

- WIFI\
    ssid: `sistembed`\
    password: `lastessa`

- MQTT\
    (il gateway della rete wifi di cui sopra, usare `WiFi.gatewayIP()`)

Ogni modulo dovrebbe anche reagire ad alcune sollecitazioni MQTT (messaggi codificati, documentati nel `man` del modulo) in modo da poterlo "forzare" in caso di intoppi nella catena.


## State diagram (generico)

(cfr. https://mermaid.js.org/syntax/stateDiagram.html)

```mermaid
stateDiagram
    AzioneInterna: Show-off, a piacere e fantasia
    [*] --> AttendoInput: comando-ON
    AttendoInput --> AzioneInterna: InputRicevuto
    AzioneInterna --> AttivaOutput
    AttivaOutput --> AttendoInput: reset (potenzialmente manuale)
    AttendoInput --> [*]: comando-OFF
```

Lo stato va pubblicato via MQTT.


## MQTT templating

Messaggistica minima da rispettare per integrarsi nel sistema.

La `root` dei messaggi sarà per tutti "**EmbeddedRubeGoldberg**"

`Identificativo` del modulo: a piacere, basta che sia univoco, es. usare il proprio *nick* o *repo_name*.

Sintassi qui sotto è: `topic` `payload`

### OUT

- STATO corrente: `<root>/<identificativo>/stato <stato attuale>`
    - stato attuale (payload): riferito al proprio `state diagram` (che può anche essere quello standard)

### IN

- TRIGGER (per far scattare l'azione in caso di mancata ricezione dell'IN o altri intoppi): `<root>/<identificativo>/trigger` (senza payload? `TBD` se vogliamo ad esempio passare un parametro, tipo un delay)


## Istruzioni per partecipare

- clonare questo repo
- creare un branch con un proprio nickname (magari un po' mnemonico)
- **lavorare sempre nel branch creato** (cmq main/master è protetto)
- creare una dir con lo stesso nome del branch
- riempire la dir con i propri file
- la prima cosa da dichiarare è l'OUTPUT che si prevede di generare
- commit&push alla bisogna (previa richiesta accesso "developer" al repo)

## "Asta" degli output

Ogni modulo, per potersi integrare con gli altri, deve negoziare input e output con DUE altri moduli (uno per IN e uno per OUT).

Mettiamo in piedi un'asta delle APPI (cfr. sopra) in cui ogni proponente dichiara (descrivendo abbastaza dettagliatamente) quale sarà l'ouput del proprio modulo, gli altri dichiareranno il proprio interesse ad usare quell'output come input.

(NB: si può fare pubblicando issues sul repo)


## Links

- https://www.slideserve.com/aldis/engg-1203-tutorial
- https://www.ccsinfo.com/content.php?page=rube-machine
- reti di Petri?
- [famoso video](https://www.youtube.com/watch?v=TzjT0aOHA4o)


## LICENZA

LIBERA!!! GPL o analoghe


## PREMI

Vorrei premiare i migliori progetti (votazione *crowd*, via *issues* sul repo) con qualche device per il contesto embedded.

Il primo classificato sceglierà per primo, il secondo per secondo, il terzo... per terzo.

Tra i seguenti:

### [Olimex](http://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-MINI/open-source-hardware) (versione con wifi)
![Olimex1](img/Olimex1.jpg)
![Olimex2](img/Olimex2.jpg)
![Olimex3](img/Olimex3.jpg)

### [Galileo](http://en.wikipedia.org/wiki/Intel_Galileo)
![Galileo1](img/Galileo1.jpg)
![Galileo2](img/Galileo2.jpg)

### [Edison](http://en.wikipedia.org/wiki/Intel_Edison)
![Edison1](img/Edison1.jpg)
![Edison2](img/Edison2.jpg)


---

---

---
## Note per chi è legato all'esame di Sistemi Embedded

La proposta del progetto nasce anche come rete di sicurezza per chi non ha idee per un progettino, a questo punto si trova "vincolato" a una specifica di massima (e al doversi integrare con altri) per cui può concentrarsi sulla realizzazione più che sulla ideazione.

Ogni modulo è meno *challenging* di un progettino che includa ad esempio un controllo a loopback (i.e., PID), ma nulla vieta di complicarlo internamente, in questo caso prediligo il tema "aderenza a specifiche hardware/software" (integrazione in progetto più ampio).

Ma il tutto funziona anche come "spazio di fantasia" per chi vuole sbizzarrirsi nel realizzare l'interno del proprio modulo inventandosi accoppiamenti strambi tra sensori e attuatori.

Chi volesse fare l'esame PRIMA del giorno della demo pubblica può farlo ovviamente.
Però dato che all'esame chiedo *in primis* di presentare il progetto e lo discutiamo, il modulo dovrà essere pronto e funzionante per poterlo esaminare e valutare.
